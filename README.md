# Présentation de l’équipe de localisation francophone à MiniDebConf Marseille 2019

## Récupérer les sources

```bash
# Clôner en HTTPS - Version anonyme
git clone --recursive https://salsa.debian.org/l10n-fr-team/talks.git

# Clôner en SSH - Nécessite d'avoir sa clé SSH sur Salsa
git clone --recursive git@salsa.debian.org:l10n-fr-team/talks.git
```

## Installer les dépendances

```bash
# Pour la version HTML
apt install pandoc

# Pour la version PDF, en plus il faut :
apt install texlive-latex-base texlive-latex-recommended texlive-latex-extra 
```

## Compiler la présentation

Sur Stretch (Debian 9) ou Buster (Debian 10) :

```bash
# Pour la version HTML
pandoc -s --slide-level=2 -t revealjs talk/talk.md -o src/2019-minidebconf-marseille.html

# Pour la version PDF
pandoc -t beamer talk/talk.md -o src/2019-minidebconf-marseille.pdf
```

## Exporter en PDF depuis la version HTML

Une fois le fichier compilé au format HTML :
- L'ouvrir avec **Chromium**,
- Ajouter `?print-pdf` à la fin de l'adresse URL **/talk.html** - [lien direct ici](https://l10n-fr-team.pages.debian.net/talks/2019-minidebconf-marseille.html?print-pdf),
- **CTRL** + **p** (pour imprimer),
- Destination : sauvegarder en PDF,
- Options : cocher la case **arrière plan graphiques**,
- Terminer en cliquant sur **Sauvegarder**.

Note : L'export au format PDF ne fonctionne pas correctement avec **Firefox**

